(*
   These functions simply add the named parameters
   that are lost during extraction
 *)
let begin_partial_application
    ~chain_id
    ~ancestor_context
    ~predecessor_timestamp
    ~predecessor_fitness
    block_header =
  begin_partial_application
    chain_id
    ancestor_context
    predecessor_timestamp
    predecessor_fitness
    block_header

let begin_application
    ~chain_id
    ~predecessor_context
    ~predecessor_timestamp
    ~predecessor_fitness
    block_header =
  begin_application
    chain_id
    predecessor_context
    predecessor_timestamp
    predecessor_fitness
    block_header

let begin_construction
    ~chain_id
    ~predecessor_context
    ~predecessor_timestamp
    ~predecessor_level
    ~predecessor_fitness
    ~predecessor
    ~timestamp
    ?protocol_data
    () =
  begin_construction
    chain_id
    predecessor_context
    predecessor_timestamp
    predecessor_level
    predecessor_fitness
    predecessor
    timestamp
    None
