#!/bin/bash

# this script assumes that you have:
# - installed fstar with opam
# - downloaded a binary of z3 and put it in your PATH
# - the tezos sources in ../tezos/

set -e

EXTRACT='Main'

rm -rf out _build proto_bla

#verify and extract code
cd fstar-code
fstar.exe --use_hints --use_hint_hashes --extract "$EXTRACT" --odir ../out --codegen OCaml main.fst
cd ..

# clean up extracted code, this should disappear with time
cd out
sed 's/Prims.parse_int "\([0-9]*\)"/\1/g' Main.ml | \
sed 's/open Prims\|Prims.\|FStar_Pervasives_Native.//g' | \
sed 's/(\([^,]*\), \([^,]*\)) tuple2/(\1 * \2)/g' | \
sed 's/Updater2/Updater/g' | \
sed 's/RPC_directory.empty ()/RPC_directory.empty/g' > Main.ml-clean
# adding the wrapper for labeled parameters
cat Main.ml-clean ../static/main-wrapper.ml > main.ml
cd ..


# craft a json input for the rpc
# tezos-client rpc post /injection/protocol with "$(cat ../proto_bla/out/input.json)"
# curl -v -H "Content-Type: application/json" -d @../proto_alpha/out/input.json http://127.0.0.1:18731/injection/protocol
cd out
# ocaml files must be in hex
cat main.ml | od -An -t x1 | tr -d ' \n' > main.hex

echo -n '{ "expected_env_version": 0,
  "components": [{ "name" : "Main" ,
                  "implementation" : "' > input.json
cat main.hex >> input.json
echo '" } ] }' >> input.json
cd ..

#prepare the directory
mkdir -p proto_bla
cp -r static/lib_protocol proto_bla/
cp out/main.ml proto_bla/lib_protocol/src/main.ml

read -p "Replace in tezos?"
rm -rf ../tezos/src/proto_bla
cp -r proto_bla ../tezos/src/

## the protocol can be tested in the following 3 ways
# 1. compile it with dune
# cd ../tezos
# dune build @install # TODO isn't there a more specific target?

# 2. compile it with the tezos compiler
# ./tezos-protocol-compiler -no-hash-check src/proto_bla/lib_protocol/src/

# 3. link it inside the node adding tezos-embedded-protocol-bla to
# src/bin_node/dune and launching the node in sandbox, only then the protocol
# is loaded. This is useful for encodings bugs for example.
