module Main //this is actually Protocol

open Error_monad

// protocol
  val max_block_length: int

  val max_operation_data_length: int

  val validation_passes: list Updater.quota

  val block_header_data : Type0

  val block_header_data_encoding: Data_encoding.t block_header_data

  noeq type block_header = {
    shell: Block_header.shell_header ;
    protocol_data: block_header_data ;
  }
  val block_header_metadata : Type0

  val block_header_metadata_encoding: Data_encoding.t block_header_metadata

  val operation_data : Type0

  val operation_receipt : Type0

  noeq type operation = {
    shell: Operation.shell_header ;
    protocol_data: operation_data ;
  }

  val operation_data_encoding: Data_encoding.t operation_data

  val operation_receipt_encoding: Data_encoding.t operation_receipt

  val operation_data_and_receipt_encoding:
    Data_encoding.t (operation_data * operation_receipt)

  val acceptable_passes: operation -> list int

  val compare_operations: operation -> operation -> int

  val validation_state : Type0

  val current_context: validation_state -> Lwt.t (tzresult Context.t)

  val begin_partial_application:
    chain_id: Chain_id.t ->
    ancestor_context: Context.t ->
    predecessor_timestamp: Time.t ->
    predecessor_fitness: Fitness.t ->
    block_header ->
    Lwt.t (tzresult validation_state)

  val begin_application:
    chain_id: Chain_id.t ->
    predecessor_context: Context.t ->
    predecessor_timestamp: Time.t ->
    predecessor_fitness: Fitness.t ->
    block_header ->
    Lwt.t (tzresult validation_state)

  val begin_construction:
    chain_id: Chain_id.t ->
    predecessor_context: Context.t ->
    predecessor_timestamp: Time.t ->
    predecessor_level: Int32.t ->
    predecessor_fitness: Fitness.t ->
    predecessor: Block_hash.t ->
    timestamp: Time.t ->
    protocol_data: option block_header_data -> // this was optional ?protocol_data
    Lwt.t (tzresult validation_state)

  val apply_operation:
    validation_state ->
    operation ->
    Lwt.t (tzresult (validation_state * operation_receipt))

  val finalize_block:
    validation_state ->
    Lwt.t (tzresult (validation_result * block_header_metadata))

  val rpc_services: RPC_directory.t Updater2.rpc_context

  val init:
    Context.t -> Block_header.shell_header -> Lwt.t (tzresult validation_result)
