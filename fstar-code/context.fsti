module Context

type t

type key = list string

type value = MBytes.t

val mem: t -> key -> Lwt.t bool
val dir_mem: t -> key -> Lwt.t bool

val get: t -> key -> Lwt.t (option value)

val set: t -> key -> value -> Lwt.t t

val copy: t -> from:key -> to_:key -> Lwt.t (option t)

val del: t -> key -> Lwt.t t
val remove_rec: t -> key -> Lwt.t t

// made up
type path =
  | Key of key
  | Dir of key

val fold:
  t -> key -> init:'a ->
  f:(path -> 'a -> Lwt.t 'a) ->
  Lwt.t 'a

val keys: t -> key -> Lwt.t (list key)
val fold_keys:
  t -> key -> init:'a -> f:(key -> 'a -> Lwt.t 'a) -> Lwt.t 'a

val register_resolver:
  Base58.encoding 'a -> (t -> string -> Lwt.t (list 'a)) -> unit

val complete: t -> string -> Lwt.t (list string)
