module Data_encoding

type t 'a
type encoding = t
type field 'a

val unit : encoding unit

val req : string -> encoding 'a -> field 'a
val obj2 : field 'a -> field 'b -> encoding ('a * 'b)
