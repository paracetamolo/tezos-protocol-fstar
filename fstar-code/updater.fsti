module Updater

// shadows the Fstar_int32 module to use ours, not sure why it works
module Int32 = Int32

noeq type validation_result = {
  context : Context.t ;
  fitness : Fitness.t ;
  message : option string ;
  max_operations_ttl : int ;
  last_allowed_fork_level : Int32.t ;
}

type quota = {
  max_size : int ;
  max_op : option int;
}

val activate : Context.t -> Protocol_hash.t -> Lwt.t Context.t
val fork_test_chain :
  Context.t ->
  protocol:Protocol_hash.t -> expiration:Time.t -> Lwt.t Context.t
