module Lwt

type t 'a

val return : 'a -> t 'a
