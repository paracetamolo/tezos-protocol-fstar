module RPC_directory

type t 'a
type directory = t

val empty : directory 'a
