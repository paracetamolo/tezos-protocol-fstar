module Main // this is actually Protocol
open Error_monad

// use my own Int32, why is this working?!
module Int32 = Int32

// protocol

let max_block_length = 0
let max_operation_data_length = 0

let validation_passes = []

let block_header_data = unit
let block_header_data_encoding = Data_encoding.unit

// noeq type block_header = {
//   shell: Block_header.shell_header ;
//   protocol_data: block_header_data ;
// }

type block_header_metadata = unit
let block_header_metadata_encoding = Data_encoding.unit

type operation_data = unit

type operation_receipt = unit

// noeq type operation = {
//   shell: Operation.shell_header ;
//   protocol_data: operation_data ;
// }

let operation_data_encoding = Data_encoding.unit
let operation_receipt_encoding = Data_encoding.unit
let operation_data_and_receipt_encoding =
  Data_encoding.obj2
    (Data_encoding.req "data" Data_encoding.unit)
    (Data_encoding.req "receipt" Data_encoding.unit)

let acceptable_passes _op = []
let compare_operations _ _ = 0



type validation_state = Context.t

let current_context vs = return vs

let begin_partial_application
    chain_id
    ancestor_context
    predecessor_timestamp
    predecessor_fitness
    block_header =
      return ancestor_context

let begin_application
    chain_id
    predecessor_context
    predecessor_timestamp
    predecessor_fitness
    block_header =
      return predecessor_context

let begin_construction
    chain_id
    predecessor_context
    predecessor_timestamp
    predecessor_level
    predecessor_fitness
    predecessor
    timestamp
    protocol_data =
      return predecessor_context

let apply_operation vs _ = return (vs, ())

open Updater

let finalize_block vs =
  return ({
    context = vs ;
    fitness = [] ;
    message = None ;
    max_operations_ttl = 0 ;
    last_allowed_fork_level = Int32.zero ;
  }, ())

let rpc_services = RPC_directory.empty

let init ctxt block_header =
  return ({
    context = ctxt ;
    fitness = [] ;
    message = None ;
    max_operations_ttl = 0 ;
    last_allowed_fork_level = Int32.zero ;
  })
