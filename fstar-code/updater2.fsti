module Updater2

// added this module to avoid a clash with the record validation_result

noeq type rpc_context = {
  block_hash : Block_hash.t;
  block_header : Block_header.shell_header;
  context : Context.t;
}
