module Error_monad

type tzresult 'a

val return : 'a -> Lwt.t (tzresult 'a)
